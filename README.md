linz-pflueckt: Template für Wordpress
=========================================

Obst ist ein Thema (Template) für Wordpress. Es basiert auf `_s`, or [`underscores`](http://underscores.me/).

Sie können es auf https://linz.pflueckt.at/ in Aktion sehen.

Das Thema ist um AJAX Funktionalität erweitert, das heisst, alle Seitenaufrufe aktualiseren nur einen Teil des HTML DOM-Baumes ohne aber die Seite neu zu laden. Dies ist notwendig, um die Karte im Hintergrund nicht neu zu laden und auf der aktuellen Auswahl zu belassen.

## Installation

1. Installieren Sie die neueste Version von Wordpress auf ihren Server, zB. http://localhost/obst 

2. Installieren Sie folgende Wordpress Plugins:
 * 404 Redirection
 * Add Meta Tags
 * Ajax Edit Comments
 * Akismet
 * Comment Images
 * Contact Form 7
 * Contact Form DB
 * GD Star Rating
 * Google XML Sitemaps
 * Nav Menu Roles
 * User Avatar
 * WP-Cycle

3. Installieren Sie das [Wordpress Template](https://gitlab.com/geraldo1/linz-pflueckt-wordpress-theme) von Gitlab als Wordpress Theme.

4. Importieren Sie die Bäume mithilfe des [Import Templates](https://gitlab.com/geraldo1/linz-pflueckt-wordpress-theme/import-template.php) von GitHub nach Wordpress.

5. Standardmässig verwenden wir eine statische geojson Datei um die Bäume auf der Karte einzuzeichnen. Um diese Datei aktuell zu halten empfiehlt es sich die Daten regelmässig, etwa durch einen cron-job oder durch einen Wordpress hook, zu exportieren. Dazu gibt stellen wir dieses [Export Template](https://gitlab.com/geraldo1/linz-pflueckt-wordpress-theme/export-template.php) bereit.

6. Testen Sie die Installation und dokumentieren Sie mögliche Fehler in den [Issues auf Gitlab](https://gitlab.com/geraldo1/linz-pflueckt-wordpress-theme/issues)


## Lizenz

Dieses Wordpress Theme ist freie Software und steht unter der [GPL license](license.txt).
