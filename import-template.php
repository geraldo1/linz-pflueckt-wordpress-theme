<?php
/**
	Template Name: Import
*/

$lines = file("https://linz.pflueckt.at/static/parse/FME_BaumdatenBearbeitet_OGD_20200806.csv");

// filter and mapping Name -> Kategorie
$KategorieArray = [
	'Apfel' => 'Apfel',
	'Apfelquitte' => 'Sonstiges',
	'Apfelquitte' => 'Sonstiges',
	'Baumhasel' => 'Nüsse',
	'Birne' => 'Birne',
	'Birnenquitte' => 'Sonstiges',
	'Birnequitte' => 'Sonstiges',
	'Blut-Pflaume' => 'Zwetschke',
	'Breitblättrige Mehlbeere' => 'Sonstiges',
	'Chinesische Wildbirne' => 'Birne',
	'Chinesischer Apfel' => 'Apfel',
	'Christusdorn' => 'Sonstiges',
	'Dreilappige Indianerbanane' => 'Sonstiges',
	'Eberesche' => 'Sonstiges',
	'Eberesche "Autumn Spire"' => 'Sonstiges',
	'Echte Mehlbeere' => 'Sonstiges',
	'Edelkastanie' => 'Edelkastanie',
	'Eingriffliger Weißdorn' => 'Sonstiges',
	'Elsbeere' => 'Sonstiges',
	'Essigbaum' => 'Sonstiges',
	'Felsenbirne' => 'Birne',
	'Flügelnuss' => 'Nüsse',
	'Fruchkirsche' => 'Kirsche',
	'Fruchtkirsche' => 'Kirsche',
	'Fruchtmaroni' => 'Edelkastanie',
	'Fruchtpflaume' => 'Zwetschke',
	'Fruchtweichsel' => 'Kirsche',
	'Fruchtzwetschke' => 'Zwetschke',
	'Fruchweichsel' => 'Kirsche',
	'Fudschijama-Kirsche' => 'Kirsche',
	'Gefülltblühende Vogel-Kirsche' => 'Kirsche',
	'Goldfrüchtiger Weißdorn' => 'Sonstiges',
	'Götterbaum' => 'Sonstiges',
	'Großblütige Trauben-Kirsche' => 'Kirsche',
	'Große Walnuss' => 'Nüsse',
	'Großfrüchtiger Wacholder' => 'Sonstiges',
	'Hänge-Kirsche' => 'Kirsche',
	'Hasel' => 'Nüsse',
	'Herbstblühende Schnee-Kirsche' => 'Kirsche',
	'Higan Kirsche' => 'Kirsche',
	'Japan. Blüten-Kirsche' => 'Kirsche',
	'Japan. Säulen-Kirsche' => 'Kirsche',
	'Japanische Alpen-Kirsche' => 'Kirsche',
	'Japanische Aprikose' => 'Sonstiges',
	'Japanische Blüten-Kirsche' => 'Kirsche',
	'Kakibaum' => 'Sonstiges',
	'Kanadische Felsenbirne' => 'Birne',
	'Kaukasische Flügelnuss' => 'Nüsse',
	'Kirsch-Pflaume' => 'Zwetschke',
	'Kirsche' => 'Kirsche',
	'Krieche' => 'Zwetschke',
	'Kugel-Steppen-Kirsche' => 'Kirsche',
	'Kulturapfel' => 'Apfel',
	'Kulturbirne' => 'Birne',
	'Kulurapfel' => 'Apfel',
	'Kupfer - Felsenbirne' => 'Birne',
	'Mährische Eberesche' => 'Sonstiges',
	'Mandelbaum' => 'Nüsse',
	'Marille' => 'Sonstiges',
	'Maulbeerbaum' => 'Sonstiges',
	'Mehl-/Vogelbeere'=> 'Sonstiges',
	'Mirabelle' => 'Zwetschke',
	'Mispel' => 'Sonstiges',
	'Mostbirne' => 'Birne',
	'Nashi-Birne' => 'Birne',
	'Nelken-Kirsche' => 'Kirsche',
	'Nuss' => 'Nüsse',
	'Österreichische Mehlbeere' => 'Sonstiges',
	'Pekanuss' => 'Nüsse',
	'Pfirsich' => 'Sonstiges',
	'Pflaumenblättriger Weißdorn' => 'Sonstiges',
	'Platanenblättriger Maulbeerbaum' => 'Sonstiges',
	'Portugiesische Lorbeer-Kirsche' => 'Kirsche',
	'Quitte' => 'Sonstiges',
	'Ringlotte' => 'Zwetschke',
	'Robinie' => 'Sonstiges',
	'Rosarote Akazie' => 'Sonstiges',
	'Rosen-Akazie' => 'Sonstiges',
	'Rotblättrige Trauben-Kirsche' => 'Kirsche',
	'Rotdorn' => 'Sonstiges',
	'Roter Maulbeerbaum' => 'Sonstiges',
	'Sanddorn' => 'Sonstiges',
	'Säulen-Robinie' => 'Sonstiges',
	'Säulenwacholder' => 'Sonstiges',
	'Schlehdorn' => 'Zwetschke',
	'Schwarzer Holler' => 'Sonstiges',
	'Schwarzer Maulbeerbaum' => 'Sonstiges',
	'Schwarznuss' => 'Nüsse',
	'Schwedische Mehlbeere' => 'Sonstiges',
	'Spanischer Wacholder' => 'Sonstiges',
	'Späte Trauben-Kirsche' => 'Kirsche',
	'Stein-Weichsel' => 'Kirsche',
	'Thüringische Mehlbeere' => 'Sonstiges',
	'Thüringische Säulen-Mehlbeere' => 'Sonstiges',
	'Tibet-Kirsche' => 'Kirsche',
	'Trauben-Kirsche' => 'Kirsche',
	'Ussuri-Birne' => 'Birne',
	'Virginische Rotblättrige Trauben-Kirsche' => 'Kirsche',
	'Vogel-Kirsche' => 'Kirsche',
	'Vogelbeere' => 'Sonstiges',
	'Walnuss' => 'Nüsse',
	'Weißdorn' => 'Sonstiges',
	'Weißer Maulbeerbaum' => 'Sonstiges',
	'Wild-Apfel' => 'Apfel',
	'Wildapfel' => 'Apfel',
	'Wildbirne' => 'Birne',
	'Wildfpflaume' => 'Zwetschke',
	'Wildkirsche' => 'Kirsche',
	'Wildpflaume' => 'Zwetschke',
	'Woll-Apfel' => 'Apfel',
	'Yoshino-Kirsche' => 'Kirsche',
	'Zweigriffliger Weißdorn' => 'Sonstiges',
	'Zwetschke' => 'Zwetschke',
];

$i = 1;
foreach ($lines as $baumid => $line) {
	if ($baumid > 1 && $baumid < 1) {
		$tokens = explode(';',$line);

		$flaeche = trim($tokens[0]);
		$baumnr = trim($tokens[1]);
		$gattung = trim($tokens[2]);
		$art = trim($tokens[3]);
		if ($art == '.') $art = '';
		$sorte = trim($tokens[4]);
		if ($sorte == '.') $sorte = '';
		$sorte = str_replace('\'', '', $sorte);

		$title = trim($tokens[5]);
		$title = str_replace('"', '', $title);
		$title = str_replace('\'', '', $title);

		$hoehe = trim($tokens[6]);
		$schirm = trim($tokens[7]);
		$stamm = trim($tokens[8]);
		$long = trim($tokens[12]);
		$lat = trim($tokens[13]);

		$category = "";
		foreach ($KategorieArray as $kat => $kategorie) {
			if (startsWith($title, $kat)) {

				echo $title.":".$kategorie."<br>";

				//insert posts into wordpress
				$post = array(
					'post_author' => 1,
					'post_name' => $i,
					'post_status' => 'publish',
					'post_title' => $title,
					'post_type' => 'baum',
				);

				//write to WP
				$id = wp_insert_post($post);
				echo "SAVED WITH ID:".$id."<br>";

				//add meta data
				add_post_meta($id, 'baumid', $i);
				add_post_meta($id, 'lat', $lat);
				add_post_meta($id, 'long', $long);
				add_post_meta($id, 'gattung', $gattung);
				add_post_meta($id, 'art', $art);
				add_post_meta($id, 'sorte', $sorte);
				add_post_meta($id, 'kat', $kategorie);
				add_post_meta($id, 'baumhoehe', $hoehe);
				add_post_meta($id, 'schirmdurchmesser', $schirm);
				add_post_meta($id, 'stammumfang', $stamm);

				$i++;

				break;
			}
		}
	}
}

function startsWith( $haystack, $needle ) {
     $length = strlen( $needle );
     return substr( $haystack, 0, $length ) === $needle;
}

?>
