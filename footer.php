<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package obst
 */
?>

	</div><!-- #main -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/extlink.min.js" type="text/javascript"></script>

</body>
</html>
